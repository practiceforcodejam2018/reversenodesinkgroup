#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int    data;
    struct node_structure *next;
}node_struct;

class LinkedList
{
    private:
/*********************************************************************************************************************************************************************************************************************************
 Method Name : createNodeWithData
 Parameters  : numData (int)
 Return Type : node_stuct* (Node Structure Type)
 Description : Creates Node with the specified number data in parameter. And returns the created node.
*********************************************************************************************************************************************************************************************************************************/
        node_struct* createNodeWithData(int numData)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data = numData;
                newNode->next = NULL;
            }
            return newNode;
        }
    
/*********************************************************************************************************************************************************************************************************************************
 Method Name : reverseTheLinkedListBetweenTheNodes
 Parameters  : startNode (node_struct* (Node structure pointer type)) , endNode (node_struct* (Node structure pointer type))
 Return Type : node_stuct* (Node Structure Pointer Type)
 Description : Reverse the list nodes between the specified start and end nodes in the parameter. And returen the new Head between those nodes.
 *********************************************************************************************************************************************************************************************************************************/
        node_struct* reverseTheLinkedListBetweenTheNodes(node_struct *startNode , node_struct *endNode)
        {
            node_struct *headNode        = startNode;
            node_struct *movingNode      = startNode;
            node_struct *nextNode        = movingNode->next;
            node_struct *terminationNode = endNode->next;
            while (nextNode != terminationNode)
            {
                movingNode->next = nextNode->next;
                nextNode->next   = headNode;
                headNode         = nextNode;
                nextNode         = movingNode->next;
            }
            return headNode;
        }

    public:
/*********************************************************************************************************************************************************************************************************************************
 Method Name : createLinkedListFromVector (Public API).
 Parameters  : dataListVector (vector of integers)
 Return Type : node_srtuct* (Node Structure Type)
 Description : Creates the complete linked list of the specified list of integer vectors in the parameter. And returns the head node of the linked list.
 *********************************************************************************************************************************************************************************************************************************/
        node_struct* createLinkedListFromVector(vector<int> dataListVector)
        {
            node_struct *headNode      = NULL;
            node_struct *insertionNode = NULL;
            int dataListVectorLength   = (int)dataListVector.size();
            for(int i = 0 ; i < dataListVectorLength ; i++)
            {
                if (!headNode)
                {
                    headNode      = createNodeWithData(dataListVector[i]);
                    insertionNode = headNode;
                }
                else
                {
                    insertionNode->next = createNodeWithData(dataListVector[i]);
                    insertionNode       = insertionNode->next;
                }
            }
            return headNode;
        }
    
/*********************************************************************************************************************************************************************************************************************************
 Method Name : displayLinkedListWithHead (Public API).
 Parameters  : head (node_struct* (Node Structure Type))
 Return Type : void
 Description : Displays the Linked list with the specified Head Node.
 *********************************************************************************************************************************************************************************************************************************/
        void displayLinkedListWithHead(node_struct *headNode)
        {
            while (headNode)
            {
                cout<<headNode->data;
                if(headNode->next)
                {
                    cout<<"->";
                }
                headNode = headNode->next;
            }
            cout<<endl;
        }
    
/*********************************************************************************************************************************************************************************************************************************
 Method Name : reverseLinkedListWithHeadAtEverySpecifiedInterval (Public API).
 Parameters  : headNode (node_struct* (Node Structure Type)) , interval (int) , sizeOfLinkedList (int) (required for checking it with interval)
 Return Type : node_struct* (Node Structure Type)
 Description : Reverse the Linked list at every specified 'interval'. And returns the new Head node of the reversed Linked List.
 *********************************************************************************************************************************************************************************************************************************/
        node_struct* reverseLinkedListWithHeadAtEverySpecifiedInterval(node_struct *headNode , int interval , int sizeOfLinkedList)
        {
            int         currentIntervalCount = 0;
            node_struct *holdingNode         = NULL;
            node_struct *savedHeadNode       = NULL;
            if(interval > sizeOfLinkedList)
            {
                cout<<"Linked list can not be reversed beyond its size. So the final list is:\n";
                return headNode;
            }
            cout<<endl<<"List after reversing nodes at every "<<interval<<" interval."<<endl<<endl;
            while (headNode)
            {
                node_struct *startNode = headNode;
                while (currentIntervalCount < interval-1)
                {
                    ++currentIntervalCount;
                    headNode = headNode->next;
                    if(!headNode)
                    {
                        return savedHeadNode;
                    }
                }
                currentIntervalCount = 0;
                savedHeadNode        = !savedHeadNode ? headNode : savedHeadNode;
                if(!holdingNode)
                {
                    reverseTheLinkedListBetweenTheNodes(startNode , headNode);
                }
                else
                {
                    holdingNode->next = reverseTheLinkedListBetweenTheNodes(startNode , headNode);
                }
                holdingNode = startNode;
                headNode    = holdingNode->next;
            }
            return savedHeadNode;
        }
};


//Driver Method
int main(int argc, const char * argv[])
{
    vector<int> dataListVector      = {1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10};
    LinkedList  linkedListObject    = LinkedList();
    node_struct *linkedListHeadNode = linkedListObject.createLinkedListFromVector(dataListVector);
    linkedListObject.displayLinkedListWithHead(linkedListHeadNode);
    linkedListHeadNode              = linkedListObject.reverseLinkedListWithHeadAtEverySpecifiedInterval(linkedListHeadNode , 6 , (int)dataListVector.size());
    linkedListObject.displayLinkedListWithHead(linkedListHeadNode);
    return 0;
}
